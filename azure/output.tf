output "ranges" {
  value = local.ranges
}

output "ranges_ipv4" {
  value = local.ranges
}

output "ranges_ipv6" {
  value = {}
}

output "all_services" {
  value = local.all_services
}

output "all_regions" {
  value = local.all_regions
}


