locals {
    aws_ip_ranges = jsondecode(file("${path.module}/ServiceTags.json"))

    all_regions = toset([ for i in local.aws_ip_ranges.values: lower(i.properties.region)])
    all_services = toset([ for i in local.aws_ip_ranges.values: lower(i.properties.systemService)])

    services = var.services == null ? local.all_services : [for i in var.services: lower(i)]
    regions = var.regions == null ? local.all_regions : [for i in var.regions: lower(i)]

    by_region = {
        for i in local.aws_ip_ranges.values: i.properties.region => i... if contains(local.regions, lower(i.properties.region))
    }

    ranges = {
        for region, i in local.by_region: lower(region) => {
            for j in i: lower(j.properties.systemService) => j.properties.addressPrefixes... if contains(local.services, lower(j.properties.systemService))
        }
    }

}

