#!/bin/bash

md5sum gcp/cloud.json > gcp/md5sum
md5sum aws/ip-ranges.json > aws/md5sum
md5sum gcp/cloud.json > azure/md5sum

curl -s -o gcp/cloud.json https://www.gstatic.com/ipranges/cloud.json
curl -s -o aws/ip-ranges.json https://ip-ranges.amazonaws.com/ip-ranges.json
#curl -o azure/ServiceTags.json https://download.microsoft.com/download/7/1/D/71D86715-5596-4529-9B13-DA13A5DE5B63/ServiceTags_Public_20210208.json

curl -o azure/ServiceTags.json $(curl -Lfs "https://www.microsoft.com/en-us/download/confirmation.aspx?id=56519" | grep -Eoi '<a [^>]+>' | grep -Eo 'href="[^\"]+"' | grep "download.microsoft.com/download/" | grep -m 1 -Eo '(http|https)://[^"]+')

md5sum gcp/cloud.json > gcp/md5sum_new
md5sum aws/ip-ranges.json > aws/md5sum_new
md5sum azure/ServiceTags.json > azure/md5sum_new

diff gcp/md5sum gcp/md5sum_new && echo "GCP didn't change" || git add gcp/cloud.json 
diff aws/md5sum aws/md5sum_new && echo "AWS didn't change" || git add aws/ip-ranges.json
diff azure/md5sum azure/md5sum_new && echo "Azure didn't change" || git add azure/ServiceTags.json

git clean -df
git commit -m"$(date +%Y-%m-%d) - $CI_JOB_ID Update ip ranges [skip ci]" || true