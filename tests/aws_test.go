package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformHelloWorldExample(t *testing.T) {
	// Construct the terraform options with default retryable errors to handle the most common
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		// Set the path to the Terraform code that will be tested.
		TerraformDir: "../examples/aws",
	})

	// Clean up resources with "terraform destroy" at the end of the test.
	defer terraform.Destroy(t, terraformOptions)

	// Run "terraform init" and "terraform apply". Fail the test if there are any errors.
	terraform.InitAndApply(t, terraformOptions)

	// Run `terraform output` to get the values of output variables and check they have the expected values.
	aws_services := terraform.OutputList(t, terraformOptions, "aws_services")
	assert.Len(t, aws_services, 19)
	assert.Contains(t, aws_services, "s3")
	assert.Contains(t, aws_services, "dynamodb")
	assert.Contains(t, aws_services, "workspaces_gateways")
	assert.Contains(t, aws_services, "route53")

	aws_regions := terraform.OutputList(t, terraformOptions, "aws_regions")
	assert.Len(t, aws_regions, 30)
	assert.Contains(t, aws_regions, "ap-south-2")
	assert.Contains(t, aws_regions, "cn-northwest-1")
	assert.Contains(t, aws_regions, "eu-west-1")
	assert.Contains(t, aws_regions, "me-south-1")
}
