output "ranges" {
  value = local.ipv4_ranges
}

output "ranges_ipv4" {
  value = local.ipv4_ranges
}

output "ranges_ipv6" {
  value = local.ipv6_ranges
}

output "all_services" {
  value = local.all_services
}

output "all_regions" {
  value = local.all_regions
}


