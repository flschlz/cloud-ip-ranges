variable "regions" {
    type = list(string)
    default = null
}

variable "services" {
    type = list(string)
    default = null
}