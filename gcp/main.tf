locals {
  gcp_ip_ranges = jsondecode(file("${path.module}/cloud.json"))

  all_regions  = toset([for i in local.gcp_ip_ranges.prefixes : lower(i.scope)])
  all_services = toset([for i in local.gcp_ip_ranges.prefixes : lower(i.service)])

  services = var.services == null ? local.all_services : [for i in var.services : lower(i)]
  regions  = var.regions == null ? local.all_regions : [for i in var.regions : lower(i)]


  by_region = {
    for i in local.gcp_ip_ranges.prefixes : i.scope => i... if contains(local.regions, lower(i.scope))
  }

  ipv4_ranges = {
    for region, i in local.by_region : lower(region) => {
      for j in i : lower(j.service) => j.ipv4Prefix... if contains(keys(j), "ipv4Prefix") && contains(local.services, lower(j.service))
    }
  }

  ipv6_ranges = {
    for region, i in local.by_region : lower(region) => {
      for j in i : lower(j.service) => j.ipv6Prefix... if contains(keys(j), "ipv6Prefix") && contains(local.services, lower(j.service))
    }

  }

}

