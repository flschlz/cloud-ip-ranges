locals {
    aws_ip_ranges = jsondecode(file("${path.module}/ip-ranges.json"))

    all_regions = toset([ for i in local.aws_ip_ranges.prefixes: lower(i.region)])
    all_services = toset([ for i in local.aws_ip_ranges.prefixes: lower(i.service)])

    services = var.services == null ? local.all_services : [for i in var.services: lower(i)]
    regions = var.regions == null ? local.all_regions : [for i in var.regions: lower(i)]


    by_region = {
        for i in local.aws_ip_ranges.prefixes: i.region => i... if contains(local.regions, lower(i.region))
    }

    ranges = {
        for region, i in local.by_region: lower(region) => {
            for j in i: lower(j.service) => j.ip_prefix... if contains(local.services, lower(j.service))
        }
    }

}

