module "aws" {
  source = "./aws"
}

output "aws_ranges" {
  value = module.aws.ranges
}

output "aws_services" {
  value = module.aws.all_services
}

output "aws_regions" {
  value = module.aws.all_regions
}

module "azure" {
  source = "./azure"
}

output "azure_ranges" {
  value = module.azure.ranges
}

output "azure_services" {
  value = module.azure.all_services
}

output "azure_regions" {
  value = module.azure.all_regions
}

